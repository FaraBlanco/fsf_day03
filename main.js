/**
 * Created by Fara Aileen on 30/3/2016.
 */
// To run this program type: node main.js
var express = require("express"); // returns an express object that would be used to create an application
var app = express(); //

// Use this directory as the directory root (not really, but for now accept this meaning)
app.use(express.static(__dirname + "/public"));  //static file will come from __dirname/public
console.log("__dirname = " + __dirname); //__dirname refers to wherever the js is executing

// Start Web server on port 3000
app.listen(3000,function() {
    console.info("My Web Server has started on port 3000");
    console.info("My document root is at " + __dirname + "/public");
    }
);